# MDR Bundle

This package includes all necessary projects to build the Samply Metadata Repository applications, including the Graphical User Interface and REST interface.

## Build

First configure Maven according to https://maven.imbei.uni-mainz.de/external.html.

Before you can build the application, it is necessary to build and install the maven POMs:

```
mvn -P parent clean install
```

This will build and install four POMs into your local maven repository. Now you can build the Metadata Repository:

```
mvn clean install
```

This will compile and install all projects. The important files are:

```
samply.mdr.gui/target/mdr-gui-$VERSION.war
samply.mdr.rest/target/mdr-rest-$VERSION.war
```

## Installation

Before you deploy this application, install and configure the required components:

* Postgresql (>=9.3)
    * With a user that has write-access to a database
* Tomcat (>=7)
    * the role `mdr-admin` and a user with this role is required to access the admin interface (`/admin/`), use the `tomcat-users.xml` to configure the role and user.

Here is a sample `tomcat-users.xml`

```xml
<?xml version='1.0' encoding='utf-8'?>
<tomcat-users>
    <role rolename="mdr-admin"/>
    <user username="admin" password="kdkjweijofwe12390yxc" roles="mdr-admin"/>
</tomcat-users>
```

Configure the password to your needs.

### Configuration

The application uses the following files:

* `/etc/samply/mdr.postgres.xml` for the database configuration
* `/etc/samply/mdr.oauth2.xml` for the central authentication configuration

Windows:

Set the Registry Key `HKEY_LOCAL_MACHINE\SOFTWARE\SAMPLY\ConfDir` to a path on your hard drive, e.g. `C:\Samply\` and put the following configuration files in there:

* `mdr.postgres.xml` for the database configuration
* `mdr.oauth2.xml` for the central authentication configuration

#### Database (`/etc/samply/mdr.postgres.xml`)

Here is a sample database configuration file:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<postgresql
    xmlns="http://schema.samply.de/config/PostgreSQL"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://schema.samply.de/config/PostgreSQL http://schema.samply.de/config/PostgreSQL.xsd">

    <host>localhost</host>

    <database>the database</database>

    <username>the database user</username>

    <password></password>

</postgresql>
```

If your PostgreSQL server listens on a different port than the default port, you
can append the port in the `host` tag, e.g. `localhost:9586`. Please make sure,
that the database exists and the user has write access in this database (the
user must be able to create new tables).

#### Central authentication (`/etc/samply/mdr.oauth2.xml`)

The central authentication mechanism (OpenID-Connet) used in this project relies on the public key of the central authentication. Also your instance must be a registered client with a client ID and a client secret, without those values you can not use this application. Once you get the public key, you must convert it into a base64 encoded X509 public key format.

```
openssl genrsa 4096 2>/dev/null | openssl rsa -pubout -outform DER 2>/dev/null | base64
```

The output of this one-line-command is an acceptable format. Currently the MDR does not support loading the public key on startup, although it is possible.

Here is a sample authentication configuration file:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<oAuth2Client
    xmlns="http://schema.samply.de/config/OAuth2Client"
    xmlns:xml="http://www.w3.org/XML/1998/namespace"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://schema.samply.de/config/OAuth2Client http://schema.samply.de/config/OAuth2Client.xsd">

    <host>https://auth.samply.de</host> <!-- The default hostname -->
    <hostPublicKey>
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAoBDqAcCKQGEbxnsJrjKdumhMCPhpOktx
VL2I/PzNjC6LjXontnsZKpETk9flcJ105Fuf3tf2tgVlJ8AxV2MavTVeaq1iXbiWs2XMnXMisfDs
Jf7yutMqgriaE97X6GHYJ05oQxVP3/9rWxMztimsm1EZY8vH5lYwcCJWNKNA4EnvWP27Kvhu9DCy
DNClw4DMOp28jy4YyGiVOzSSdZ7kd/73t21KI1mg/TeNenyQ6aFAK+UmgEK9TjSB1OV1vmBzbXar
QwbvxSEbys2pfTu2BQ08i4TQK/vipZYLDx1BMjJqQfB+2uTrjgStc51Wiw3GMM8dshFr6ZZ76kyv
xbw4uII/Azhsdc5EftG3o9tx4UHnyUrrqT2j9sNKQn7zTM6Xbj8C04YaaUovZj7cugCadm16WErY
6ZbqdEhCqvC583e4cURf3grQSlMUqo7IA/cJ6wohAWUGo0nGrseLPwldR0lB7uvPHF3c2OdmE3wF
diaKZlU1L6jC4hzZpcFzkVAA5DqjFZP7ksX3Q7E/BvoEe1RjMYqj5OLOl3QZbiysOaNXqSxzRe8j
aqDh/w1pDH/eNAEL9n81/Y/fHs1VCY4RlwHYMzIeP2hQIDMoR/mBHzcCcgDiar6cIXHRc7tL2n4B
AaYUSD2MwIu98XRQr+6G88APC9J7Q0D07LPLDpXhpPMCAwEAAQ==
    </hostPublicKey>
    <clientId>your-client-id</clientId>
    <clientSecret>your-client-secret</clientSecret>

    <!-- Additional hostnames depend on the servername that is used in the request. Don't use this if you don't need it. -->
    <additionalHostnames>
        <hostname>
            <host>https://auth.samply.de</host>
            <ifServernameEquals>mdr.samply.de</ifServernameEquals>
        </hostname>
        <hostname>
            <host>https://auth.ccpit.dktk.dkfz.de</host>
            <ifServernameEquals>mdr.ccpit.dktk.dkfz.de</ifServernameEquals>
        </hostname>
    </additionalHostnames>

</oAuth2Client>
```

## Deployment

Copy the following files

```
samply.mdr.gui/target/mdr-gui-$VERSION.war
samply.mdr.rest/target/mdr-rest-$VERSION.war
```

into the webapps folder of your application server and rename them if required, e.g. `mdr-gui-$VERSION.war` to `ROOT.war`, `mdr-rest-$VERSION.war` to `v2.war`. This way the web application is accessible with the URL `$HOST/` and the REST interface is accessible with the URL `$HOST/v2/`.

See [this website](https://tomcat.apache.org/tomcat-8.0-doc/config/context.html) for more informations about context containers in tomcat.

Start the server.

### Central authentication

With this setup the MDR is running, but it is not possible to login because the central authentication service does not know your application. In order to register your new MDR in Samply Auth, please write us an email with the following informations:

- The URL(s) to the graphical user interface of your MDR (e.g. `https://mdr.hochschule-schildhausen.de:8443/mdr/`), `localhost` is acceptable in a development environment only (e.g. `http://localhost:8080/`)

We will register your client in our application and send you an email containing:

- your client ID
- your client secret
- the public key of the central authentication service (the key above is just an example)
- the URL of the central authentication service

The authentication with DFN-AAI is not possible yet. we are still working on it.

## Usage

Open your web browser and navigate to the application, e.g. `$HOST/`. The admin interface is available under the `/admin/` path, e.g. `$HOST/admin` or `http://localhost:8080/admin/`.
